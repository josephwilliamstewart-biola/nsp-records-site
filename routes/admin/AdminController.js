import * as STATUS_CODES from 'http-status-codes';
import {
	returnSQLResponse,
	getPermissions,
} from '../util';

export const updateWitnessingEvent = (request, response, connection) => {

	let { id } = request.params;

	if (!getPermissions(request).isAdmin) {
		response.status(STATUS_CODES.UNAUTHORIZED).send('You do not have admin status');
		return;
	} else if (isNaN(parseInt(id))) {
		response.status(STATUS_CODES.BAD_REQUEST).send('Improper id supplied');
		return;
	} else {

		let { changes } = request.body;
		let statements = getUpdateStatementFromChangeSet(changes);
		let sqlStat = `
			 UPDATE witnessing_day SET
			 ${statements} WHERE id = ${id}`;

		connection.query(
			sqlStat,
			returnSQLResponse(connection, response, () => ({status: 200, reason: 'OK'}))
		)
	}

}

export const updateRallyEvent = (request, response, connection) => {

	let { id } = request.params;

	if (!getPermissions(request).isAdmin) {
		response.status(STATUS_CODES.UNAUTHORIZED).send('You do not have admin status');
		return;
	} else if (isNaN(parseInt(id))) {
		response.status(STATUS_CODES.BAD_REQUEST).send('Improper id supplied');
		return;
	} else {

		let { changes } = request.body;
		let statements = getUpdateStatementFromChangeSet(changes);
		let sqlStat = `
			UPDATE rallies SET
		 	${statements} WHERE id = ${id}`

		connection.query(
			sqlStat,
			returnSQLResponse(connection, response, () => ({status: 200, reason: 'OK'})))
	}

}

export const getContactInfo = (request, response, connection) => {
	if (!request.user.isAdmin) {
		response.status(STATUS_CODES.UNAUTHORIZED).send('You do not have admin status');
	}

	let { id } = request.params

	connection.query(
		`SELECT speaker_email, speaker_phone, performer_phone, performer_email
    	FROM rallies
    	WHERE id = ${id}`,
		returnSQLResponse(connection, response)
	)
}
