import React, {Component} from 'react';

/**
 * @description A button used to collapse and expand modules
 */
class CollapseButton extends Component {
	constructor(props) {
		super(props);

		this.state = {
			collapsed: props.collapsed,
			collapsed_label: props.collapsed_label,
			raised: props.raised,
			uncollapsed_label: props.uncollapsed_label
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.collapsed != nextProps.collapsed) {
			this.setState(
				{
					collapsed: nextProps.collapsed
				}
			)
		}
	}

	render() {
		return (
			<button className={this.state.raised ? 'mdl-button--raised mdl-button mdl-js-button mdl-button--accent' : 'mdl-button mdl-js-button mdl-button--accent'} onClick={this.props.onClick}>
				{this.state.collapsed ? this.state.collapsed_label : this.state.uncollapsed_label}
			</button>
		)
	}
}

export default CollapseButton;