import React from 'react';

/**
 * @description Paginated table that takes a regular HTML table as it's child
 */
export default class PageTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index:0,
      max:this.props.max
    }

    this.superMax = 5;
    this.len = 0;
  }

  changeMax(event) {
    this.setState({
      max:event.target.value,
      index:0
    })
  }

  left() {
    if (this.state.index > 0) {
      this.setState({
        index:this.state.index-1
      })
    }
  }

  right() {
    if(this.state.index < (Math.ceil(this.len/(this.state.max==null ? 10 : this.state.max))-1)) {
      this.setState({
        index:this.state.index+1
      })
    }
  }

  linkClick(event) {
    this.setState({
      index:parseInt(event.target.dataset.ref)
    })
  }

  render() {

    let len = this.len;
    let head = [];
    let body = [];

    React.Children.forEach(this.props.children,(child,i) => {
      if (child.type == "tbody") {
        len = React.Children.count(child.props.children);
        body = React.Children.toArray(child.props.children);
      } else if (child.type == "thead") {
        head = React.Children.toArray(child.props.children);
      }
    })

    let max = this.state.max == null ? 10 : this.state.max;
    let buts = [];

    if (Math.ceil(len/max) > 1) {
      buts.push(<a className="pagetable-link" onClick={this.left.bind(this)} href="#"><i className="material-icons">arrow_left</i></a>)
      if (Math.ceil(len/max) <= this.superMax) {
        for (var i = 0; i < Math.ceil(len/max); i++) {
          buts.push(<a className={`pagetable-link ${this.state.index==i ? "selected" : ""}`} onClick={this.linkClick.bind(this)} data-ref={i} href="#">{i+1}</a>)
        }
      } else if (this.state.index < this.superMax || this.state.index == Math.ceil(len/max)-1) {
        for (var i = 0; i < this.superMax; i++) {
          buts.push(<a className={`pagetable-link ${this.state.index==i ? "selected" : ""}`} onClick={this.linkClick.bind(this)} data-ref={i} href="#">{i+1}</a>);
        }
        buts.push(<span>-</span>);
        buts.push(<a className={`pagetable-link ${this.state.index==(Math.ceil(len/max)-1) ? "selected" : ""}`} onClick={this.linkClick.bind(this)} data-ref={Math.ceil(len/max)-1} href="#">{Math.ceil(len/max)}</a>)
      } else {
        for (var i = 0; i < this.superMax; i++) {
          buts.push(<a className="pagetable-link" onClick={this.linkClick.bind(this)} data-ref={i} href="#">{i+1}</a>);
        }
        buts.push(<span>-</span>);
        buts.push(<a className="pagetable-link selected" onClick={this.linkClick.bind(this)} data-ref={this.state.index} href="#">{this.state.index+1}</a>);
        buts.push(<span>-</span>);
        buts.push(<a className={`pagetable-link ${this.state.index==(Math.ceil(len/max)-1) ? "selected" : ""}`} onClick={this.linkClick.bind(this)} data-ref={Math.ceil(len/max)-1} href="#">{Math.ceil(len/max)}</a>)
      }
      buts.push(<a className="pagetable-link" onClick={this.right.bind(this)} href="#"><i className="material-icons">arrow_right</i></a>)
    }

    this.len = len;

    return (
      <div className="pagetable-meta-container">
        {
          Math.ceil(len/max) > 1 ?
          <div className="pagetable-buttons">
            {
              buts
            }
          </div>
          : ""
        }
        <table className = {this.props.cssClass}>
          <thead className="sticky-header">
            {
              head
            }
          </thead>
          <tbody>
            {
              body.map((e,i) => {
                if (i >= this.state.index*max && i < (this.state.index+1)*max) {
                  return e;
                } else {
                  return;
                }
              })
            }
          </tbody>
        </table>
        <div className="pagetable-buttons">
          <label htmlFor="max">Show</label>
          <select onChange={this.changeMax.bind(this)}>
            <option value="10" defaultValue>10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
        </div>
      </div>
    )

  }
}
