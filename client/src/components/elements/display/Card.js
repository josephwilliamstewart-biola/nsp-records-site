import React, {Component} from 'react';

/**
 * @description A white card to put things on
 */
class Card extends Component {
	render() {
		let css_class = `${this.props.cssClass} mdl-color--white mdl-shadow--2dp`;

		return (
			<div className={css_class + " card"}>
				{this.props.children}
			</div>
		)
	}
}

export default Card;
