import React from 'react';

/**
 * @description The box view for the ranged ministry results on the main dashboard
 */
export default class DataBox extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      title: props.title,
      titleEdit:false,

      ralliesHover:false,
      witnessingHover:false,
      schoolsHover:false
    }
  }

  toggleHover(attribute) {
    let obj = {}

    obj[attribute] = !this.state[attribute];

    this.setState(obj)
  }

  editTitle(event) {
    this.setState({
      title:event.target.value
    })
    this.props.changeTitle(event.target.value);
  }

  setEditMode(state) {
    this.setState({
      titleEdit:state
    }, () => {
      componentHandler.upgradeDom();
    })
  }

  componentDidMount() {
    componentHandler.upgradeDom();
  }

  componentDidUpdate() {
    componentHandler.upgradeDom();
  }

  render() {
    return (
      <div className="data-box-container">

        <div className="databox-control-buttons">
          <button id="edit-title" className="mdl-button mdl-js-button mdl-button--icon" onClick={() => this.setEditMode(true)}>
            <i className="material-icons">edit</i>
          </button>
          <div htmlFor="edit-title" className="mdl-tooltip">
            Edit range title
          </div>
          <button id="delete-range" className="mdl-button mdl-js-button mdl-button--icon" onClick={this.props.onRemove}>
            <i className="material-icons">cancel</i>
          </button>
          <div htmlFor="delete-range" className="mdl-tooltip">
            Delete range
          </div>
        </div>

        {
          !this.state.titleEdit ?
            <h4 className="data-box-title">
              {this.state.title}
            </h4>
          :
            <div className="data-box-title-edit">
              <form onSubmit = {(event) => {event.preventDefault(); if(this.state.title != "") this.setEditMode(false)}}>
                <div className="mdl-textfield mdl-js-textfield">
                  <input onChange={this.editTitle.bind(this)} className="mdl-textfield__input" type="text" id="titleField"/>
                  <label className="mdl-textfield__label" htmlFor="titleField">{this.state.title}</label>
                </div>
                <button className="mdl-button mdl-js-button mdl-button--icon">
                  <i className="material-icons">check_circle</i>
                </button>
              </form>
            </div>
        }
        <div className="data-box" onClick={() => this.toggleHover("ralliesHover")}>
          <div className={`data-box-content ${this.state.ralliesHover? "hover" : ""}`}>
            <p className="data-box-number-display">
              {this.props.data.allTimeRallies}
            </p>
            <p className="data-label">
              Total Rallies
            </p>
           <div className={`data-box-tooltip`}>
             <div className="data-box-tooltip-box">
               <p className="data-box-number-display-tooltip">
                 {this.props.data.allTimeRallyAttendance}
               </p>
               <p className="data-label-drop-down">
                 Total Attendance
               </p>
             </div>
              <div className="data-box-tooltip-box">
               <p className="data-box-number-display-tooltip">
                 {this.props.data.allTimeRallyIndicatedDecisions}
               </p>
               <p className="data-label-drop-down">
                 Indicated Decisions
               </p>
              </div>
             </div>
            </div>
          </div>
        <div className="data-box" onClick={() => this.toggleHover("witnessingHover")}>
          <div className={`data-box-content ${this.state.witnessingHover? "hover" : ""}`}>
            <p className="data-box-number-display">
              {this.props.data.allTimeWitnessingDays}
            </p>
            <p className="data-label">
            Total Witnessing Events
            </p>
            <div className={`data-box-tooltip`}>
              <div className="data-box-tooltip-box">
                <p className="data-box-number-display-tooltip">
                  {this.props.data.allTimePeopleApproached}
                </p>
                <p className="data-label-drop-down">
                  Approached
                </p>
              </div>
              <div className="data-box-tooltip-box">
                <p className="data-box-number-display-tooltip">
                  {this.props.data.allTimeGospelPresentations}
                </p>
                <p className="data-label-drop-down">
                  Gospel Presentations
                </p>
              </div>
              <div className="data-box-tooltip-box">
                <p className="data-box-number-display-tooltip">
                  {this.props.data.allTimeWitnessingIndicatedDecisions}
                </p>
                <p className="data-label-drop-down">
                  Indicated Decisions
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="data-box" onClick={() => this.toggleHover("schoolsHover")}>
          <div className={`data-box-content ${this.state.schoolsHover? "hover" : ""}`}>
            <p className="data-box-number-display">
              {this.props.data.allTimeSchoolsCoached}
            </p>
            <p className="data-label">
              Schools Coached
            </p>
            <div className={`data-box-tooltip`}>
              <div className="data-box-tooltip-box">
                <p className="data-box-number-display-tooltip">
                  {this.props.data.allTimeSchoolsActive}
                </p>
                <p className="data-label-drop-down">
                  Schools Active
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="data-box">
          <div className="data-box-content">
            <p className="data-box-number-display">
              {this.props.data.allTimeIndicatedDecisions}
            </p>
            <p className="data-label">
              Indicated Decisions
            </p>
          </div>
        </div>
      </div>
    )
  }

}
