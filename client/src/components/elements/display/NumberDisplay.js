import React, {Component} from 'react';
import EditableField from '../input/EditableField';

/**
 * @description A perhaps misnamed component that displays some emphasized data along with a label
 * @todo Rename this
 */
class NumberDisplay extends Component {
	constructor(props) {
		super(props);

		this.state = {
			active: false
		}
	}

	toggleActive() {
		this.setState({active:!this.state.active})
	}

	render() {
		let css_class = `${this.props.cssClass} number-display`;

		return (
			<div onClick={this.toggleActive.bind(this)} className={this.props.cssClass + ` ${this.props.tooltips != null && this.props.tooltips[0] != null ? `data-box-content ${this.state.active ? "hover" : ""}` : ""}`}>
				<div className="number-display">
					{
						this.props.editing ?
							<EditableField
									obj={this.props.obj}
									label={this.props.objLabel}
									type={this.props.isNum ? "number" : "text"}
									changes={this.props.changes}
									setState={this.props.setState}
									editing={this.props.editing}
									isAdmin={this.props.isAdmin}
								/>
						:
							<div className={`number-display--number ${this.props.num_css}`}>
									{
										this.props.number_display
									}
							</div>
					}

					<div className="number-display--label">
						{this.props.label}
					</div>
				</div>
			</div>
		)
	}
}

export default NumberDisplay;
