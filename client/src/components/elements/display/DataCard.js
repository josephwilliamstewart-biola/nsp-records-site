import React from 'react';

/**
 * @description A kind of ugly high level data display component. It is the blue box on the main dashboard.
 * @todo Redesign this whole process
 */
export default class DataCard extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      remove: false,
      title: props.title,
      editMode:false
    }
  }

  handleOnClick(event) {
    let element = event.target;
    if (element.tagName == "SPAN") {
      element = element.parentElement;
    }

    let panel = element.nextElementSibling;
    let clump = element.parentElement;

    element.classList.toggle("active");
    panel.classList.toggle("hidden");
    clump.classList.toggle("clump-hidden");
  }

  handleMouseEnter(event) {
    this.setState({
      remove:true,
    })
  }

  handleMouseLeave(event) {
    this.setState({
      remove:false
    })
  }

  componentDidUpdate() {
    componentHandler.upgradeDom();
  }

  setEditMode(editMode) {
    this.setState({
      editMode,
      remove:false,
    })
  }

  editTitle(event) {
    this.setState({
      title:event.target.value
    })
    this.props.changeTitle(event.target.value);
  }

  render() {
    return (
      <div className= "data-card">
        <div className="data-card-header" onMouseEnter={this.handleMouseEnter.bind(this)} onMouseLeave={this.handleMouseLeave.bind(this)}>
          {
            this.state.editMode ?
              <form onSubmit={() => this.setEditMode(false)}>
                <div className="mdl-textfield mdl-js-textfield">
                  <input onChange={this.editTitle.bind(this)} className="mdl-textfield__input" type="text" id="titleField"/>
                  <label className="mdl-textfield__label" htmlFor="titleField">{this.state.title}</label>
                </div>
                <button className="mdl-button mdl-js-button mdl-button--icon">
                  <i className="material-icons">check_circle</i>
                </button>
              </form>
            :
              this.state.remove ?
                <div>
                  <button id="edit-title" className="mdl-button mdl-js-button mdl-button--icon" onClick={() => this.setEditMode(true)}>
                    <i className="material-icons">edit</i>
                  </button>
                  <div htmlFor="edit-title" className="mdl-tooltip">
                    Edit range title
                  </div>
                  <button className="mdl-button mdl-js-button mdl-button--icon" onClick={this.props.onRemove}>
                    <i className="material-icons">cancel</i>
                  </button>
                </div>
              :
                <h5>{this.state.title}</h5>
          }
        </div>

        {
          this.props.config.map(
            (config,i) => {
              let main = config.main;
              let children = config.children;

              return (
                <div className = "data-card-section">
                  <button className="data-card-accordian active" onClick={this.handleOnClick.bind(this)}>
                    <span className="data-card-desc">{main.display}:</span> <span className="data-card-value-main">{this.props.obj[main.data]}</span>
                  </button>
                  <div className = "data-card-panel">

                    {
                      children.map(
                        (child,j) => {
                          return <div className = {`data-card-panel-section ${j == children.length-1 && i == this.props.config.length-1 ? "data-card-bottom-border" : ""}`}>
                            <span className="data-card-desc">{child.display}:</span> <span className="data-card-value-sub">{this.props.obj[child.data]}</span>
                          </div>
                        }
                      )
                    }

                  </div>
                </div>
              )
            }
          )
        }

        {
          this.props.footer != null ?
            <div className="data-card-footer">
              <span className="data-card-desc">{this.props.footer.display}:</span> <span className="data-card-value-footer">{this.props.obj[this.props.footer.data]}</span>
            </div>
          :
            ''
        }
      </div>
    )
  }

}
