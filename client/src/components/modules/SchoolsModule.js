import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import _ from 'lodash';
import 'whatwg-fetch';

import PageTable from '../elements/display/PageTable'
import Card from '../elements/display/Card';
import NumberDisplay from '../elements/display/NumberDisplay';
import CollapseButton from '../elements/input/CollapseButton';

class SchoolsModule extends Component {
	constructor(props) {
		super(props);

		this.state = {
			chapter_ids: props.chapter_ids,
			collapsed: props.collapsed,
			current_range_school_count: props.schools.length,
			schools: props.schools,
			sort_by: props.sort_by,
			unique_school_array: [],
			unique_school_count: 0,
			max_date: props.max_date,
			min_date: props.min_date,
			reverse_sort_by: false
		};

		this.handleCollapseState = this.handleCollapseState.bind(this);
		this.handleSortBy = this.handleSortBy.bind(this);
	}

	handleCollapseState() {
		this.setState(
			{
				collapsed: !this.state.collapsed
			}
		)
	}

	removeAllClassesFromElements(nodes) {
		for (let index = 0; index < nodes.length; index++) {
			let node = nodes[index];

			node.classList = '';
		}
	}

	handleSortBy(sort_by, event) {
		this.removeAllClassesFromElements(event.target.parentNode.children);

		if (sort_by == this.state.sort_by) {
			let newSortByOrder = !this.state.reverse_sort_by;

			this.setState(
				{
					reverse_sort_by: newSortByOrder,
					schools: _.orderBy(this.state.schools, [`${this.state.sort_by}`], [`${newSortByOrder ? 'desc' : 'asc'}`])
				}
			)

			event.target.classList.add(`mdl-data-table__header--sorted-${newSortByOrder ? 'descending' : 'ascending'}`);
		}
		else {
			this.setState(
				{
					reverse_sort_by: false,
					sort_by: sort_by,
					schools: _.orderBy(this.state.schools, [`${sort_by}`], [`asc`])
				}
			);

			event.target.classList.add(`mdl-data-table__header--sorted-ascending`);

		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.chapter_ids != nextProps.chapter_ids) {
			this.setState(
				{
					chapter_ids: nextProps.chapter_ids
				}
			)

			this.getUniqueSchoolCountForAllTime();
		}

		if (this.props.schools != nextProps.schools) {
			this.setState(
				{
					current_range_school_count: nextProps.schools.length,
					schools: _.orderBy(nextProps.schools, [`${this.state.sort_by}`], [`${this.state.reverse_sort_by ? 'desc' : 'asc'}`])
				}
			)
		}

		if (nextProps.collapsed != this.props.collapsed) {
			this.setState(
				{
					collapsed: nextProps.collapsed
				}
			)
		}

		if (nextProps.max_date != this.props.max_date) {
			this.setState(
				{
					max_date: nextProps.max_date
				}
			)
		}

		if (nextProps.min_date != this.props.min_date) {
			this.setState(
				{
					min_date: nextProps.min_date
				}
			)
		}
	}

	componentDidMount() {
		this.getUniqueSchoolCountForAllTime();
	}

	getUniqueSchoolCountForAllTime() {
		fetch(`/api/schools/school_id/distinct/chapter_ids/${this.state.chapter_ids.toString()}`,{credentials: 'include'})
			.then(response => response.json())
			.then(
				school_count => {
					this.setState(
						{
							unique_school_count: school_count.length,
							unique_school_array: school_count
						}
					);
				}
			);
	}

	render() {
		return (
			<Card cssClass={this.props.cssClass}>
				<div className="mdl-grid mdl-grid-centered">
					<div className="mdl-cell mdl-cell--2-col">
						<h4>Schools</h4>
					</div>

					<div className="mdl-cell mdl-cell--10-col mdl-grid">
						<div className="mdl-cell">
							<NumberDisplay
								num_css="importance-4"
								label="All time unique schools"
								number_display={this.state.unique_school_count.toLocaleString()}
							/>
						</div>

						<div className="mdl-cell">
							<NumberDisplay
								num_css="importance-4"
								label="Unique Schools in range"
								number_display={this.state.current_range_school_count.toLocaleString()}
							/>
						</div>
					</div>
				</div>
				{
					this.state.current_range_school_count > 0 ?
						<div className="mdl-grid">
								<CollapseButton
									collapsed={this.state.collapsed}
									collapsed_label="More"
									onClick={this.handleCollapseState}
									raised={false}
									uncollapsed_label="Less"
								/>

								{!this.state.collapsed ?
								<PageTable cssClass='mdl-data-table mdl-js-data-table mdl-cell mdl-cell--12-col'>
									<thead>
										<tr>
											<th onClick={(event) => this.handleSortBy('name', event)}>Name</th>
											<th onClick={(event) => this.handleSortBy('county', event)}>County</th>
											<th onClick={(event) => this.handleSortBy('state', event)}>State</th>
											<th onClick={(event) => this.handleSortBy('chapter_name', event)}>Chapter</th>
										</tr>
									</thead>
									<tbody>
										{this.state.schools.map(
											(school, index) => {
												let school_address = `https://www.google.com/maps/place/${school.name}/${school.address}`;

												return <tr key={index}>
													<td>
														<Link to={`/school/${school.school_id}`}>
															{school.name}
														</Link>
													</td>

													<td>
														<a href={school_address} target="_blank">
															{school.county}
														</a>
													</td>

													<td>
														{school.state}
													</td>

													<td>
														<Link to={`/chapter/custom?ids=${school.chapter_id}&min_date=${this.state.min_date}&max_date=${this.state.max_date}&query_title=${school.chapter_name}`}>
															{school.chapter_name}
														</Link>
													</td>
												</tr>
											}
										)}
									</tbody>
								</PageTable>
								:''}
						</div>
						: ''
				}
			</Card>
		)
	}
}

export default SchoolsModule;
