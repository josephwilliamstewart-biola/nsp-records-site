import React from 'react';
import NumberDisplay from '../elements/display/NumberDisplay';
import Card from '../elements/display/Card';
import CollapseButton from '../elements/input/CollapseButton';

export default class BasicModule extends React.Component {

  /*
      props
      -----
      showing - whether or not the module is being displayed
      obj - object to pull data from
      title- title of module
      headerElements - description of elements to go on the header
        headerElement[i] = {
            display,
            data,
            importance, // 1 being low 5 being high
            isNum, //Boolean value describing if element is number
          }
  */
  constructor(props) {
    super(props);

    this.state = {
      collapsed: this.props.collapsed
    }
  }

  handleCollapseState() {
    this.setState({
      collapsed:!this.state.collapsed
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      collapsed:nextProps.collapsed
    });
  }

  render() {
    const block = {
      "display": "block",
      "textAlign": "center"
    }

    if (this.props.showing || this.props.editing) {
      return (

        <div className = "mdl-grid">
          <div className = "mdl-cell mdl-cell--12-col">
            <Card>
              <div className="mdl-grid mdl-grid-centered">
                <div className="mdl-cell mdl-cell--2-col">
                  <div style={block}>
                    <h4>{this.props.title}</h4>
                    {
                      this.props.children != null ?
                      <CollapseButton
                        collapsed={this.state.collapsed}
                        collapsed_label="More"
                        onClick={this.handleCollapseState.bind(this)}
                        raised={false}
                        uncollapsed_label="Less"
                      />
                      :""
                    }
                  </div>
                </div>
                {this.props.headerElements.map(
                  (o,index) => {
                    let tooltips = [];
                    if (o.tooltips != null) {
                      tooltips = o.tooltips.map(
                        (tooltip) => {
                          if (typeof tooltip.data == "function") {
                            return {display:tooltip.display,data:tooltip.data(this.props.obj)};
                          } else {
                            return {display:tooltip.display,data:tooltip.data};
                          }
                        }
                      )
                    }
                    return <NumberDisplay cssClass="mdl-cell mdl-cell--2-col"
                              label={o.display}
                              num_css = {`importance-${o.importance != null ? o.importance : "3"} ${this.props.obj[o.data] != null && this.props.obj[o.data] != "" ? "" : "fade"}`}
                              number_display={this.props.obj[o.data] != null && this.props.obj[o.data] != "" ? this.props.obj[o.data] : "NA"}
                              tooltips={tooltips}
                              changes={this.props.changes}
                              isAdmin={this.props.isAdmin}
                              setState={this.props.setState}
                              editing={this.props.editing}
                              objLabel={o.data}
                              obj={this.props.obj}
                              isNum={o.isNum}
                            />
                  }
                )}
              </div>

              {
                (!this.state.collapsed || this.props.editing) && this.props.children != null ?
                <div className="mdl-grid">
                  <div className="mdl-cell mdl-cell--12-col">
                    {this.props.children}
                  </div>
                </div>
                : ""
              }
            </Card>
          </div>
        </div>
      )
    } else {
      return (<div></div>);
    }
  }

}
