import React from 'react';
import {Link} from 'react-router-dom';
import Card from '../../elements/display/Card';
import DatePicker from '../../elements/input/DatePicker';
import BasicModule from '../../modules/BasicModule';
import CollapseButton from '../../elements/input/CollapseButton';
import EditableField from '../../elements/input/EditableField';
import moment from 'moment';
import 'whatwg-fetch';
import SkyLight from 'react-skylight';
import Gallery from 'react-photo-gallery';
import Lightbox from 'react-images';

export default class WitnessingDashboard extends React.Component {

  constructor(props) {
    super(props)

    let {id} = props.match.params;

    this.state = {
      id,
      obj: null,
      allCollapsed: true,
      isLoading: true,
      requestmessage: "",
      suggestedEdit: "",
      isAdmin: false,
      isEdit: false,
      schools: [],
      chapters: [],
      changes: {},
      currentImage: 0
    }

    this.impactHeader = [
      {
        display: "People Approached",
        data: "approached",
        importance:5,
        isNum: true
      },
      {
        display: "Gospel Presentations",
        data: "gospel_presentations",
        importance:5,
        isNum: true
      },
      {
        display:"First Time Decisions",
        data: "indicated_decisions",
        importance:5,
        isNum: true
      },
      {
        display: "Conversations With Christians",
        data: "conversation_with_christians",
        isNum: true
      },
      {
        display:"Recommittments",
        data: "recommittments",
        isNum: true
      }
    ]

    this.locationHeader = [
      {
        display:"Location",
        data:"location"
      },
      {
        display:"Time",
        data:"time_of_day",
        importance:2
      },
      {
        display:"Time Spent",
        data:"witnessing_time_spent",
        importance:2,
      }
    ]

    this.peopleHeader = [
      {
        display:"Collected Data",
        data:"collected_data"
      },
      {
        display:"Students",
        data:"hs_student_witness",
        isNum: true
      },
      {
        display:"NSP Members",
        data:"csp_members",
        isNum: true
      }
    ]

    this.materialsHeader = [
      {
        display:"Gospel Booklets",
        data:"gospel_booklets",
        isNum: true
      },
      {
        display:"Lifebooks",
        data:"lifebooks",
        isNum: true
      },
      {
        display:"Gospels of John",
        data:"gospels_of_john",
        isNum: true
      },
      {
        display:"Bibles",
        data:"bibles",
        isNum: true
      }
    ]
  }

  openLightbox(event, obj) {
    this.setState({
      currentImage: obj.index,
      lightboxIsOpen: true,
    });
  }
  closeLightbox() {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  }
  gotoPrevious() {
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  }
  gotoNext() {
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  }

  fetchWitnessing() {
    fetch(`/api/witnessing_days/${this.state.id}`,{credentials: 'include'}).then(response => response.json()).then(
      witnessing => {
        this.setState({
          obj:witnessing[0],
          isLoading:false
        }, () => {
          this.getUser();
        });
      }
    )
  }

  componentDidMount() {
    this.fetchWitnessing();
  }

  handleCollapseAllState() {
    this.setState(
      {
        allCollapsed: !this.state.allCollapsed
      }
    )
  }

  textFieldChange(event) {
    this.setState(
      {
        suggestedEdit: event.target.value
      }
    )
  }

  getUser() {
    fetch("/api/user",{credentials:"include"}).then(response => {
      return response.json()
    }).then(
      user => {
        this.setState({
          isAdmin: user.isAdmin
        }, () => {
          if (this.state.isAdmin) {
            this.getChapterNames();
            this.getSchoolNames();
          }
        })
      }
    )
  }

  getChapterNames() {
    fetch("/api/chapters", {credentials:"include"}).then((response) => {
      return response.json();
    }).then(
      chapters => {
        this.setState({
          chapters: chapters.map((obj,index) => {
            return {
              display:obj.name,
              val:obj.id
            }
          })
        })
      }
    )
  }

  getSchoolNames() {
    fetch(`/api/admin/getschools/${moment(this.state.obj.date).format("YYYY-MM-DD")}`,{credentials:"include"}).then((response) => {
      return response.json();
    }).then(
      schools => {
        this.setState({
          schools: schools.map((obj,index) => {
            return {
              display: obj.name,
              schoolId: obj.id,
              regId: obj.registration_id
            }
          })
        })
      }
    )
  }

  submitSuggestedEdit() {
    this.requestEditModal.hide();

    fetch(`/api/suggestedit/witnessingday/${this.state.id}/${this.state.suggestedEdit}`, {credentials:'include'}).then(response => {
      return response.json()
    }).then(
      response => {
        if (response.error) {
          this.setState({
            requestmessage:"Your request failed to send, feel free to email IT directly to perform the desired action.",
            submittingRequest:false,
          })
          this.modal.show();
        } else {
          this.setState({
            requestmessage:"Your request was successfully submitted. It may take a while to process.",
            submittingRequest:false,
          })
          this.modal.show();
        }
      }
    )
  }

  postChanges() {
    fetch(`/api/admin/witnessing/${this.state.id}`, {credentials:"include",method:"POST",headers: {
        'Content-Type':'application/json'
      },body:JSON.stringify({changes:this.state.changes})}).then(
      (response) => response.json()
    ).then(
      (response) => {
        if (response.status == 200) {
          this.setState({
            requestmessage:"Your change was made!",
          })
          this.modal.show();
        } else if (response.status == 401) {
          this.setState({
            requestmessage:"Impostor!! You are no admin!!",
          })
          this.modal.show();
        } else if (response.status == 500) {
          this.setState({
            requestmessage:"My sincerest apologies, it appears as if the server made an oopsies..."
          })
          this.modal.show();
        }
        this.setState({
          changes: {}
        })
      }
    )
  }

  handleEdit() {
    if (!this.state.isAdmin) {
      this.requestEditModal.show()
    } else if (!this.state.isEdit) {
      this.setState({
        isEdit:true
      })
    } else if (this.state.changes != {}) {
      this.postChanges()
      this.setState({
        isEdit:false
      })
    } else {
      this.setState({
        isEdit:false
      })
    }
  }

  cancelEdit() {
    this.setState({
      isEdit:false,
      changes:{}
    })
  }

  componentDidUpdate() {
    componentHandler.upgradeDom();
  }

  render() {

    let photos = []
    if (this.state.obj != null && typeof this.state.obj.picture_url == "string") {
      photos = this.state.obj.picture_url.split(",").map(
        url => {
          if (!url.includes("http://") && !url.includes("https://"))
            return {
              src: `https://drive.google.com/uc?export=view&id=${url}`,
              width: 1,
              height: 1,
            }
          else
            return {
              src: url,
              width: 1,
              height: 1
            }
        }
      )
    }

    if (!this.state.isLoading && this.state.obj != null) {
      return (
        <div>
          <Card cssClass="mdl-grid mdl-grid-centered mdl-cell--12-col">

            <h3 className="mdl-layout-title mdl-cell mdl-cell--1-col">Witnessing Details (#{this.state.id})</h3>
            <div className="mdl-layout-spacer"></div>
            <h3 className="mdl-layout-title mdl-cell mdl-cell--8-col">
              {
                !this.state.isEdit ?
                  <Link to={`/school/${this.state.obj.school_id}`}>{this.state.obj.school_name}</Link>
                :
                  <EditableField
                      obj={this.state.obj}
                      label="school_name"
                      labels={{
                        display: {
                          obj: true,
                          objId: "school_name"
                        },
                        regId: {
                          change: true,
                          changeId: "schoolreg_id"
                        },
                        schoolId: {
                          obj: true,
                          objId: "school_id"
                        }
                      }}
                      type="select"
                      options={this.state.schools}
                      changes={this.state.changes}
                      setState={this.setState.bind(this)}
                      editing={this.state.isEdit}
                      isAdmin={this.state.isAdmin}
                    />

              }
               {" "}({
                 !this.state.isEdit ?
                  <Link to={`/chapter/custom?ids=${this.state.obj.chapter_id}&query_title=${this.state.obj.chapter_name}`}>{this.state.obj.chapter_name}</Link>
                :
                  <EditableField
                      obj={this.state.obj}
                      label="chapter_name"
                      labels={{
                        display: {
                          obj: true,
                          objId: "chapter_name"
                        },

                        val: {
                          obj: true,
                          objId: "chapter_id",
                          changeId: "chapter_id",
                          change: true
                        }
                      }}
                      valueLabel="chapter_id"
                      type="select"
                      options={this.state.chapters}
                      changes={this.state.changes}
                      setState={this.setState.bind(this)}
                      editing={this.state.isEdit}
                      isAdmin={this.state.isAdmin}
                    />
               })</h3>
            <h3 className="mdl-layout-title mdl-cell mdl-cell--2-col">{
              !this.state.isEdit ?
                moment(this.state.obj.date.substring(0,10)).format('MM/DD/YYYY')
              :
                <EditableField
                    obj={this.state.obj}
                    label="date"
                    type="date"
                    changes={this.state.changes}
                    setState={this.setState.bind(this)}
                    editing={this.state.isEdit}
                    isAdmin={this.state.isAdmin}
                  />
            }</h3>
          </Card>

          <div className="mdl-grid">
            <div className="mdl-cell--10-col mdl-cell">
              <button style={{marginRight:"10px;"}} className="mdl-button mdl-js-button mdl-button--raised" onClick={this.handleEdit.bind(this)}>
                {
                  !this.state.isAdmin ?
                    "Request Edit"
                  : this.state.isAdmin && !this.state.isEdit ?
                    "Edit"
                  :
                    "Finish Editing"
                }
              </button>
              {
                this.state.isAdmin && this.state.isEdit ?
                  <button className="mdl-button mdl-js-button mdl-button--raised" onClick={this.cancelEdit.bind(this)}>
                    Cancel Edit
                  </button>
                :
                  ""
              }
            </div>

            <div className="mdl-cell--2-col mdl-cell">
              <CollapseButton
                collapsed={this.state.allCollapsed}
                collapsed_label="Show All"
                onClick={this.handleCollapseAllState.bind(this)}
                raised={true}
                uncollapsed_label="Hide All"
              />
            </div>
          </div>

          <BasicModule
            showing={(this.state.obj.stories != "" && this.state.obj.stories != null) || this.state.isEdit}
            obj={this.state.obj}
            headerElements = {[]}
            title="Story"
            collapsed={this.state.allCollapsed}
            editing={this.state.isEdit}
            changes={this.state.changes}
            isAdmin={this.state.isAdmin}
            setState={this.setState.bind(this)}
          >
            <EditableField
                obj={this.state.obj}
                label="stories"
                type="textfield"
                changes={this.state.changes}
                setState={this.setState.bind(this)}
                editing={this.state.isEdit}
                isAdmin={this.state.isAdmin}
              />
          </BasicModule>

          <BasicModule
            showing={true}
            obj={this.state.obj}
            headerElements = {this.impactHeader}
            title="Impact"
            collapsed = {this.state.allCollapsed}
            editing = {this.state.isEdit}
            changes={this.state.changes}
            isAdmin={this.state.isAdmin}
            setState={this.setState.bind(this)}
          >
            <ul>
              <li>
                <p>There were{" "}
                  <span className="emphasis">
                    <EditableField
                        obj={this.state.obj}
                        label="spirit_filled_life"
                        type="number"
                        changes={this.state.changes}
                        setState={this.setState.bind(this)}
                        editing={this.state.isEdit}
                        isAdmin={this.state.isAdmin}
                      />
                  </span>
                {" "}indicated desires to be filled with the Holy Spirit</p>
              </li>
              <li>
                <p>There were{" "}
                  <span className="emphasis">
                    <EditableField
                        obj={this.state.obj}
                        label="follow_up_requests"
                        type="number"
                        changes={this.state.changes}
                        setState={this.setState.bind(this)}
                        editing={this.state.isEdit}
                        isAdmin={this.state.isAdmin}
                      />
                  </span>
                {" "}follow up requests</p>
              </li>
              <li>
                <p>There were{" "}
                  <span className="emphasis">
                    <EditableField
                        obj={this.state.obj}
                        label="satisfied"
                        type="number"
                        changes={this.state.changes}
                        setState={this.setState.bind(this)}
                        editing={this.state.isEdit}
                        isAdmin={this.state.isAdmin}
                      />
                    </span>
                {" "}satisfied presentations</p>
              </li>
            </ul>
          </BasicModule>

          <BasicModule
            showing={true}
            obj={this.state.obj}
            headerElements = {this.peopleHeader}
            title="People Involved"
            collapsed = {this.state.allCollapsed}
            editing = {this.state.isEdit}
            changes={this.state.changes}
            isAdmin={this.state.isAdmin}
            setState={this.setState.bind(this)}
          >
            <ul>
              <li>
                <p>There was {" "}
                    <EditableField
                        obj={this.state.obj}
                        label="others"
                        type="number"
                        changes={this.state.changes}
                        setState={this.setState.bind(this)}
                        editing={this.state.isEdit}
                        isAdmin={this.state.isAdmin}
                      />
                {" "}other people involved</p>
              </li>
              {
                 this.state.obj.hs_student_witness != null && this.state.obj.hs_student_witness != 0 ?
                  <li>
                    <p>The students who participated in this event were{" "}
                      <span className="emphasis">
                        <EditableField
                            obj={this.state.obj}
                            label="hs_student_names"
                            type="text"
                            changes={this.state.changes}
                            setState={this.setState.bind(this)}
                            editing={this.state.isEdit}
                            isAdmin={this.state.isAdmin}
                          />
                      </span>
                    </p>
                  </li>
                :
                  ""
              }
              {
                this.state.obj.csp_members != null && this.state.obj.csp_members != 0 ?
                  <li>
                    <p>The NSP members who participated in this event were{" "}
                      <span className="emphasis">
                        <EditableField
                            obj={this.state.obj}
                            label="csp_member_names"
                            type="text"
                            changes={this.state.changes}
                            setState={this.setState.bind(this)}
                            editing={this.state.isEdit}
                            isAdmin={this.state.isAdmin}
                          />
                      </span>
                    </p>
                  </li>
                :
                  ""
              }
              {
                !this.state.isEdit && (this.state.obj.training_content == null || this.state.obj.training_content == "No students were trained" || this.state.obj.training_content == "") ?
                  <li>
                      <p>No students were trained at this witnessing event</p>
                  </li>
                :
                  <li>
                    <p>The students were trained in the following (
                      <EditableField
                          obj={this.state.obj}
                          label="training_time_spent"
                          type="text"
                          changes={this.state.changes}
                          setState={this.setState.bind(this)}
                          editing={this.state.isEdit}
                          isAdmin={this.state.isAdmin}
                        />
                      )</p>
                    <ul>
                      {
                        this.state.isEdit ?
                          <li>Seperate values with comma:
                            <EditableField
                                obj={this.state.obj}
                                label="training_content"
                                type="text"
                                changes={this.state.changes}
                                setState={this.setState.bind(this)}
                                editing={this.state.isEdit}
                                isAdmin={this.state.isAdmin}
                              />
                          </li>
                        :
                          this.state.obj.training_content.split(",").map(
                            element => {
                              return <li><p>{element}</p></li>
                            }
                          )
                      }
                    </ul>
                  </li>
              }
              <li>
                {
                  this.state.isEdit ?
                    <p>There{" "}
                        <EditableField
                            obj={this.state.obj}
                            label="debrief_explaination"
                            type="select"
                            options = {[
                              "was",
                              "was not"
                            ]}
                            displayFilter = {(val) => {
                              if (val == "No debrief at all" || val == null || val == "")
                                return "was not"
                              else
                                return "was"
                            }}
                            valFilter = {(val) => {
                              if (val == "was not")
                                return "No debrief at all"
                              else
                                return "Most people in the group were able to share"
                            }}
                            changes={this.state.changes}
                            setState={this.setState.bind(this)}
                            editing={this.state.isEdit}
                            isAdmin={this.state.isAdmin}
                          />
                      {" "}debrief {
                        this.state.obj.debrief_explaination != "No debrief at all" && this.state.obj.debrief_explaination != null && this.state.obj.debrief_explaination != "" ?
                          <span> and{" "}
                            <EditableField
                                obj={this.state.obj}
                                label="debrief_explaination"
                                type="select"
                                options={[
                                  "Most people in the group were able to share",
                                  "A few people were able to share",
                                  "No debrief except for collecting information"
                                ]}
                                displayFilter={(val) => {
                                  return val.toLowerCase()
                                }}
                                changes={this.state.changes}
                                setState={this.setState.bind(this)}
                                editing={this.state.isEdit}
                                isAdmin={this.state.isAdmin}
                              />
                            </span>
                        : ""
                      }</p>
                  : this.state.obj.debrief_explaination == null || this.state.obj.debrief_explaination == "No debrief at all" || this.state.obj.debrief_explaination == "" ?
                    <p>There <span className="negative">was no</span> debrief at all</p>
                  :
                    <p>There <span className="positive">was</span> debrief and <span className = "undl">{this.state.obj.debrief_explaination.toLowerCase()}</span></p>
                }
              </li>
            </ul>
          </BasicModule>

          <BasicModule
            showing={true}
            obj={this.state.obj}
            headerElements = {this.materialsHeader}
            title="Materials"
            collapsed = {this.state.allCollapsed}
            editing = {this.state.isEdit}
            changes={this.state.changes}
            isAdmin={this.state.isAdmin}
            setState={this.setState.bind(this)}
          >
          </BasicModule>

          <BasicModule
            showing={true}
            obj={this.state.obj}
            headerElements= {this.locationHeader}
            title="Location"
            collapsed={this.state.allCollapsed}
            editing = {this.state.isEdit}
            changes={this.state.changes}
            isAdmin={this.state.isAdmin}
            setState={this.setState.bind(this)}
          >
            <ul>
              <li>
                {
                  this.state.isEdit ?
                    <span>This event{" "}
                    <EditableField
                        obj={this.state.obj}
                        label="was_location_hs"
                        type="select"
                        options={[
                          "was",
                          "was not"
                        ]}
                        valFilter={(val) => {
                          if (val == "was")
                            return "Yes"
                          else
                            return "No"
                        }}
                        displayFilter={(val) => {
                          if (val == "Yes")
                            return <span className="positive">was</span>
                          else
                            return <span className="negative">was not</span>
                        }}
                        changes={this.state.changes}
                        setState={this.setState.bind(this)}
                        editing={this.state.isEdit}
                        isAdmin={this.state.isAdmin}
                      />
                    {" "}at a high school</span>
                  : this.state.obj.was_location_hs == "Yes" ?
                    <p>This witnessing event <span className="positive">was</span> at a highschool</p>
                  :
                    <p>This witnessing event <span className="negative">was not</span> at a highschool</p>
                }
              </li>
            </ul>
          </BasicModule>

          <BasicModule
            showing={this.state.isEdit || (this.state.obj.picture_url != null && this.state.obj.picture_url != "")}
            obj = {this.state.obj}
            headerElements={[]}
            title="Pictures"
            collapsed={this.state.allCollapsed}
            editing = {this.state.isEdit}
            changes={this.state.changes}
            isAdmin={this.state.isAdmin}
            setState={this.setState.bind(this)}
          >
            <div className = "mdl-grid">
              {
                this.state.isEdit ?
                  <p>Separate picture urls by comma (for google drive pictures, just put document id):{" "}
                  <EditableField
                      obj={this.state.obj}
                      label="picture_url"
                      type="text"
                      changes={this.state.changes}
                      setState={this.setState.bind(this)}
                      editing={this.state.isEdit}
                      isAdmin={this.state.isAdmin}
                    />
                  </p>
                : this.state.obj.picture_url != null ?
                  <div>
                    <Gallery photos={photos} onClick={this.openLightbox.bind(this)} />
                  </div>

                : ""
              }
            </div>
          </BasicModule>

          <Lightbox images={photos}
            onClose={this.closeLightbox.bind(this)}
            onClickPrev={this.gotoPrevious.bind(this)}
            onClickNext={this.gotoNext.bind(this)}
            currentImage={this.state.currentImage}
            isOpen={this.state.lightboxIsOpen}
          />

          <SkyLight
            ref={ref => this.requestEditModal = ref}
            dialogStyles = {
              {
                marginTop:0,
                top:"10%",
                height:"80%",
                overflow:"auto"
              }
            }
            hideOnOverlayClicked
          >

            <label>What changes need to be made?</label>
            <textarea onChange={this.textFieldChange.bind(this)} className="mdl-textfield__input" type="text" rows= "8"></textarea>
            <br/>
            <button className="mdl-button mdl-js-button mdl-button--raised" onClick={this.submitSuggestedEdit.bind(this)}>
              Submit
            </button>
            <button className="mdl-button mdl-js-button mdl-button--raised" onClick={() => this.requestEditModal.hide()}>
              Cancel
            </button>
          </SkyLight>

          <SkyLight
            ref={ref => this.modal = ref}
            hideOnOverlayClicked
            dialogStyles = {
              {
                height:"auto"
              }
            }>

            <h3>{this.state.requestmessage}</h3>
          </SkyLight>
        </div>
      )
    } else {
      return (
        <div>
          <Card cssClass="mdl-grid mdl-grid-centered mdl-cell--12-col">
            <h3 className="mdl-layout-title mdl-cell mdl-cell--2-col">Witnessing event not found</h3>
          </Card>
        </div>
      )
    }
  }

}
