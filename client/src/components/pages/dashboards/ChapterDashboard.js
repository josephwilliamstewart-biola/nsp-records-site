import React, {Component} from 'react';

import moment from 'moment';
import queryString from 'query-string';
import 'whatwg-fetch';

import Card from '../../elements/display/Card';

import CollapseButton from '../../elements/input/CollapseButton';
import DatePicker from '../../elements/input/DatePicker';

import RalliesModule from '../../modules/RalliesModule';
import SchoolsModule from '../../modules/SchoolsModule';
import WitnessingModule from '../../modules/WitnessingModule';
import analytics from '../../../utils/analytics';
import fetch from '../../../utils/fetch';

// Import Chapter Groupings

import CSP from '../../../constants/chapter_groupings/CSP.json';
import ISP from '../../../constants/chapter_groupings/ISP.json';

const chapter_groupings_array = [
	CSP,
	ISP
];

import {
	getCurrentYearOfMinistry
} from '../../../utils/date_calculations';

class ChapterDashboard extends Component {
	constructor(props) {
		super(props);

		const parsed = queryString.parse(props.location.search);

		let ids = [];

		let {grouping_title} = props.match.params;

		let grouping_title_from_map = false;

		if (grouping_title == 'custom') {
			ids = parsed.ids.split(',');
			grouping_title = parsed.query_title;
		}
		else {
			let object = this.getTitleAndIdsForDashboard(grouping_title);
			ids = object.ids;
			grouping_title = object.grouping_title;
			grouping_title_from_map = true;
		}

		let current_ministry_year = getCurrentYearOfMinistry();

		this.state = {
			all_time_rallies_count: 0,
			all_time_witnessing_count: 0,
			chapter_ids: ids,
			chapters: [],
			collapse_all: true,
			grouping_title_from_map: grouping_title_from_map,
			grouping_title: grouping_title,
			max_date: parsed.max_date != null ? moment(parsed.max_date).format('YYYY-MM-DD') : current_ministry_year.max_date,
			min_date: parsed.min_date != null ? moment(parsed.min_date).format('YYYY-MM-DD') : current_ministry_year.min_date,
			rallies: [],
			schools: [],
			witnessing_days: []
		};

		this.handleOnClick = this.handleOnClick.bind(this);
		this.handleMaxDateStateChange = this.handleMaxDateStateChange.bind(this);
		this.handleMinDateStateChange = this.handleMinDateStateChange.bind(this);
		this.handleCollapseAllState = this.handleCollapseAllState.bind(this);
	}

	handleCollapseAllState() {
		this.setState(
			{
				collapse_all: !this.state.collapse_all
			}
		)
	}

	getTitleAndIdsForDashboard(grouping_title) {
		let return_object = {};

		for (let index = 0; index < chapter_groupings_array.length; index++) {
			let chapter_grouping = chapter_groupings_array[index];

			let {title, chapters} = chapter_grouping;

			if (title.toLowerCase().replace(/ /g, '-') == grouping_title) {
				return_object.grouping_title = title

				return_object.ids = chapters.map(chapter => chapter.id).toString();

				break;
			}
		}

		return return_object;
	}

	handleMinDateStateChange(event) {
		return this.setState(
			{
				min_date: event.target.value
			}
		);
	}

	handleMaxDateStateChange(event) {
		return this.setState(
			{
				max_date: event.target.value
			}
		);
	}

	componentDidMount() {
		this.fetchData();
	}

	fetchData = () => {
		const promises = [
			fetch(`/api/witnessing_day/${this.state.chapter_ids}?startDate=${this.state.min_date}&endDate=${this.state.max_date}`),
			fetch(`/api/schools/chapter_ids/${this.state.chapter_ids}?startDate=${this.state.min_date}&endDate=${this.state.max_date}`),
			fetch(`/api/rallies/custom/chapter_ids/${this.state.chapter_ids}?startDate=${this.state.min_date}&endDate=${this.state.max_date}`),
			analytics.count(`rallies {chapter IN (${this.state.chapter_ids})}`),
			analytics.count(`witnessing {chapter IN (${this.state.chapter_ids})}`)
		]

		Promise.all(promises).then( results => {
			this.setState({
				witnessing_days: results[0],
				schools: results[1],
				rallies: results[2],
				all_time_rallies_count: results[3] && results[3].result,
				all_time_witnessing_count: results[4] && results[4].result,
			})
		});
	}

	handleOnClick() {
		this.setState(
			{
				updating_data: true
			}
		);

		this.updateDashboardComponents();
	}

	updateDashboardComponents() {
		this.fetchData();
		this.updateURLState();
		this.setState(
			{
				updating_data: false
			}
		);
	}

	updateURLState() {
		if (this.state.grouping_title_from_map) {
			window.history.pushState(null, null, `/chapter/${this.state.grouping_title.toLowerCase().replace(/ /g, '-')}?ids=${this.state.chapter_ids}&min_date=${this.state.min_date}&max_date=${this.state.max_date}`);
		}
		else {
			window.history.pushState(null, null, `/chapter/custom?ids=${this.state.chapter_ids}&min_date=${this.state.min_date}&max_date=${this.state.max_date}&query_title=${this.state.grouping_title}`);

		}
	}

	render() {
		return (
			<div className="chapter-dashboard">
				<Card cssClass="mdl-grid mdl-grid-centered mdl-cell--12-col">
					<span className="mdl-layout-title mdl-cell mdl-cell--2-col">{
						this.state.chapters.length == 1 ?
						this.state.chapters[0].name :
						this.state.grouping_title
					}</span>

					<div className="mdl-layout-spacer"></div>

					<DatePicker
						label="Start Date"
						inputId="min_date"
						onChange={this.handleMinDateStateChange}
						value={this.state.min_date}
					/>

					<div className="mdl-layout-spacer"></div>

					<DatePicker
						label="End Date"
						inputId="max_date"
						onChange={this.handleMaxDateStateChange}
						value={this.state.max_date}
					/>

					<div className="mdl-layout-spacer"></div>

					<div className="mdl-cell mdl-cell--2-col">
						<button id="refresh" className="mdl-button mdl-js-button mdl-button--icon">
							<i className="material-icons" disabled={this.state.fetching_rallies} onClick={this.handleOnClick}>refresh</i>
						</button>
						<div htmlFor="refresh" className="mdl-tooltip">
							Refresh Dashboard
						</div>
					</div>
				</Card>

				{
					(this.state.rallies.length > 0 || this.state.schools.length > 0 || this.state.witnessing_days.length > 0) ?

					<div className="mdl-grid mdl-textfield--align-right">
						<div className="mdl-cell--10-offset-desktop mdl-cell">
							<CollapseButton
								collapsed={this.state.collapse_all}
								collapsed_label="Show All"
								onClick={this.handleCollapseAllState}
								raised={true}
								uncollapsed_label="Hide All"
							/>
						</div>
					</div>

					:

					''
				}

				<div className="mdl-grid">
					<div className="mdl-cell mdl-cell--12-col">
						<SchoolsModule
							chapter_ids={this.state.chapter_ids}
							collapsed={this.state.collapse_all}
							max_date={this.state.max_date}
							min_date={this.state.min_date}
							schools={this.state.schools}
							sort_by="school_name"
						/>
					</div>
				</div>

				<div className="mdl-grid">
					<div className="mdl-cell mdl-cell--12-col">
						<RalliesModule
							all_time_count={this.state.all_time_rallies_count}
							collapsed={this.state.collapse_all}
							rallies={this.state.rallies}
							sort_by="school_name"
						/>
					</div>
				</div>

				<div className="mdl-grid">
					<div className="mdl-cell mdl-cell--12-col">
						<WitnessingModule
							collapsed={this.state.collapse_all}
							all_time_count={this.state.all_time_witnessing_count}
							sort_by="name"
							witnessing_days={this.state.witnessing_days}
						/>
					</div>
				</div>
			</div>
		)
	}
}

export default ChapterDashboard;
