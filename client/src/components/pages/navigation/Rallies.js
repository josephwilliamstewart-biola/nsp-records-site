import React from 'react';
import PageTable from '../../elements/display/PageTable';
import Filter from '../../elements/input/Filter';
import 'whatwg-fetch';
import {Link} from 'react-router-dom';
import moment from 'moment';

export default class WitnessingEvents extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      rallies: [],
      filteredRallies: [],
      filterRegex:"id",
      showingFilter:false,
    }

    this.filterColumns = [
      {
        display:"Collected Data",
        column:"collected_data",
        type:"string"
      },
      {
        display:"Date",
        column:(obj) => {
          return obj.date.substring(0,10);
        },
        type:"date"
      },
      {
        display:"Stories",
        column:"stories",
        type:"string"
      },
      {
        display:"Type",
        column:"type",
        type:"string"
      },
      {
        display:"Week Theme",
        column:"week_theme",
        type:"string"
      },
      {
        display:"Rally Theme",
        column:"daily_theme",
        type:"string"
      },
      {
        display:"Students Involved",
        column:"student_names_participating",
        type:"string"
      },
      {
        display:"Speaker",
        column:(obj) => {
          return obj.was_speaker.toLowerCase() == "yes" || (obj.speaker_name != null && obj.speaker_name != "") ? "Yes" : ""
        },
        type:"boolean"
      },
      {
        display:"Speaker Name",
        column:"speaker_name",
        type:"string"
      },
      {
        display:"Performer",
        column:(obj) => {
          return obj.was_performer.toLowerCase() == "yes" || (obj.performer_name != null && obj.performer_name != "") ? "Yes" : ""
        },
        type:"boolean"
      },
      {
        display:"Performer Name",
        column: "performer_name",
        type:"string"
      },
      {
        display:"Attendance",
        column:"total_attendance",
        type:"number"
      },
      {
        display:"Indicated Decisions",
        column:"indicated_decisions",
        type:"number"
      }
    ]
  }

  fetchRallies() {
    fetch('/api/rallies',{credentials: 'include'}).then(response => response.json()).then(
      rallies => {
        this.setState({
          rallies,
          filteredRallies:rallies
        })
      }
    )
  }

  componentDidMount() {
    this.fetchRallies();
  }

  changeFilter(filterRegex) {
		this.refs.search_field.value = "";
		this.setState({
			filterRegex
		})
	}

  filterRallies() {
		let value = this.refs.search_field.value;
		this.setState({
			filteredRallies: _.filter(this.state.rallies,(rally) => {
				if (this.state.filterRegex != null && rally[`${this.state.filterRegex}`] != null)
					return rally[`${this.state.filterRegex}`].toString().toLowerCase().indexOf(value.toLowerCase()) !== -1;
				else
					return false;
			})
		})
	}

  toggleFilter(event) {
    this.setState({
      showingFilter: event.target.checked
    })
  }

  render() {
    return (
      <div>
        <h3 style={{"textAlign":"center"}}>Rallies</h3>

        <div className="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
          <label className="mdl-button mdl-js-button mdl-button--icon" htmlFor="search_field">
            <i className="material-icons">search</i>
          </label>
          <div className="mdl-textfield__expandable-holder">
            <input className="mdl-textfield__input" type="text" id="search_field" ref="search_field" onChange={this.filterRallies.bind(this)}/>
            <label className="mdl-textfield__label" htmlFor="search_field">Find a School</label>
          </div>
        </div>
        <label className="mdl-icon-toggle mdl-js-icon-toggle mdl-js-ripple-effect" htmlFor="filter-toggle">
          <input onChange={(event) => this.toggleFilter(event)} type="checkbox" id="filter-toggle" className="mdl-icon-toggle__input"/>
          <i className="mdl-icon-toggle__label material-icons">filter_list</i>
        </label>

        <Filter
            columns= {this.filterColumns}
            showing={this.state.showingFilter}
            setState={this.setState.bind(this)}
            resultsField="filteredRallies"
            data={this.state.rallies}
          />

        <PageTable cssClass="mdl-data-table mdl-js-data-table mdl-cell mdl-cell--12-col">
          <thead>
            <tr>
              <th className={`${this.state.filterRegex=="id" ? "filtered" : ''}`} onClick={() => this.changeFilter("id")}>ID</th>
              <th className={`mdl-data-table__cell--non-numeric ${this.state.filterRegex=="school_name" ? "filtered" : ''}`} onClick={() => this.changeFilter("school_name")}>School</th>
              <th className={`mdl-data-table__cell--non-numeric ${this.state.filterRegex=="collected_data" ? "filtered" : ''}`} onClick={() => this.changeFilter("collected_data")}>Collected Data</th>
              <th className={`mdl-data-table__cell--non-numeric ${this.state.filterRegex=="date" ? "filtered" : ''}`} onClick={() => this.changeFilter("date")}>Date</th>
              <th className={`${this.state.filterRegex=="total_attendance" ? "filtered" : ''}`} onClick={() => this.changeFilter("total_attendance")}>Attendance</th>
              <th className={`${this.state.filterRegex=="indicated_decisions" ? "filtered" : ''}`} onClick={() => this.changeFilter("indicated_decisions")}>Indicated Decisions</th>
              <th className={`mdl-data-table__cell--non-numeric ${this.state.filterRegex=="daily_theme" ? "filtered" : ''}`} onClick={() => this.changeFilter("daily_theme")}>Theme</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.state.filteredRallies.map(
              (rally, index) => {
                return <tr key={index}>
                  <td>
                    {rally.id}
                  </td>

                  <td className="mdl-data-table__cell--non-numeric">
                    <Link to={`/school/${rally.school_id}`}>
                      {rally.school_name}
                    </Link>
                  </td>

                  <td className="mdl-data-table__cell--non-numeric">
                    {rally.collected_data}
                  </td>

                  <td className="mdl-data-table__cell--non-numeric">
                    {moment(rally.date.substring(0,10)).format('MMMM Do YYYY')}
                  </td>

                  <td>
                    {rally.total_attendance}
                  </td>

                  <td>
                    {rally.indicated_decisions}
                  </td>

                  <td className="mdl-data-table__cell--non-numeric">
                    {rally.daily_theme}
                  </td>

                  <td>
                    <Link to={`/rally/${rally.id}`}>
                      <i className="material-icons">add</i>
                    </Link>
                  </td>
                </tr>
              }
            )}
          </tbody>
        </PageTable>
        {
          this.state.filteredRallies.length > 0 ?
            <footer>
              <i>{this.state.filteredRallies.length} results</i>
            </footer>
          :
            ""
        }
      </div>
    )
  }

}
