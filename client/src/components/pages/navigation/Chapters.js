import React, {Component} from 'react';
import {Link} from 'react-router-dom';

// Import Chapter Groupings
import CSP from '../../../constants/chapter_groupings/CSP.json';
import ISP from '../../../constants/chapter_groupings/ISP.json';
import {aliases} from '../../../constants/chapter_aliases.json';
import _ from 'lodash';

const chapter_groupings_map = [
	CSP,
	ISP
];

import {
	toggleIsLoading
} from '../../../utils/stateChanges';

class Chapters extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			chapters: []
		};
	}

	handleChaptersChange(chapters) {
		return this.setState({chapters: chapters});
	}

	handleLoadingStateToggle() {
		return this.setState(toggleIsLoading);
	}

	getAlias(name) {
		var chapter = _.find(aliases,o => {
			return o.name == name;
		})
		if (chapter == undefined) {
			return name;
		} else {
			return chapter.alias;
		}
	}

	componentDidMount() {
		fetch('/api/chapters',{credentials: 'include'})
			.then(response => response.json())
			.then(
				chapters => {
					this.handleLoadingStateToggle();
					this.handleChaptersChange(chapters);
				}
			);
	}

	render() {
		const centerStyle = {
			textAlign:"center"
		}
		const centerDiv = {
			margin: "0 auto"
		}
		return (
			<div style={centerDiv}>
				<h2 style={centerStyle}>Chapters</h2>
				<div className="mdl-grid-centered">

					<h5 className="mdl-cell mdl-cell--12-col">Groups:</h5>

					<ul className="mdl-grid mdl-cell mdl-cell--12-col boxy-3">
						{
							chapter_groupings_map.map(
								(chapter_grouping) => {
									return <li className="mdl-cell mdl-list__item">
										<Link
											to={{
												pathname: `/chapter/${chapter_grouping.title.toLowerCase().replace(/ /g, '-')}`
											}}
										>
											<button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">
												{chapter_grouping.title}
											</button>
										</Link>
									</li>
								}
							)
						}
					</ul>
				</div>

				<br/>
				{this.state.loading ? <div className="mdl-spinner mdl-js-spinner is-active"></div> : ''}

				<div className="mdl-grid-centered">

					<h5 className="mdl-cell mdl-cell--12-col">All Chapters:</h5>

					<ul className="mdl-grid mdl-cell mdl-cell--12-col boxy-3">
						{
							this.state.chapters.map(
								(chapter, index) => {
									return <li className="mdl-cell mdl-list__item" key={index}>
										<Link
											to={{
												pathname: `/chapter/custom`,
												search: `?ids=${chapter.id}&query_title=${chapter.name}`
											}}
											className={`${chapter.status == "Inactive" ? 'mdl-badge mdl-badge--overlap' : ''}`}
											data-badge="!"
										>
											<button className={`mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect`} data-badge="!">
												{this.getAlias(chapter.name)}
											</button>
										</Link>
									</li>
								}
							)
						}
					</ul>
				</div>
			</div>
		)
	}
}

export default Chapters;
